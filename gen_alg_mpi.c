#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <mkl.h>
#include <mpi.h>

/*This algorithm works with constant function 
 * if you want to change function you have to
 * change some parts of the programm 
 * Current function is << y(x) = sin(x)*sin(y)*sin(z) >>  //CHANGE IF FUNC CHANGED!!!
 * This programm founds << minimum >> of the function
 * pop_vect is p1_x1, p2_x1, ... , p10_x1, p1_x2, ... , p10_xn*/
 
enum 
{
	DIM = 3,                                         //function dimention
	POP_SIZE = 10000,                                   //population size
	ITER_COUNT = 1000,                                 //break criterion
	STEP_COUNT = 32,                                  //change criterion
};


int main()
{
	MPI_Init(NULL, NULL);
	double *pop_vect = (double*)malloc(sizeof(double)*(DIM*POP_SIZE)), *rand_vect = (double*)malloc(sizeof(double)*(DIM*POP_SIZE*2));
	int i, j, current_iter = 0, step = 0, N_MAX, rank, step_count;
	double minimum, y, result; 
	double *new_pop_vect = (double*)malloc(sizeof(double)*(DIM*POP_SIZE)), *tmp_buf = (double*)malloc(sizeof(double)*(DIM*POP_SIZE));                     
	MPI_Status *status;													   //for changing
	
	MPI_Comm_size(MPI_COMM_WORLD, &N_MAX);         // count of processors
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	step_count = STEP_COUNT / N_MAX;
	
//random
	VSLStreamStatePtr stream;
	vslNewStream(&stream, VSL_BRNG_MT2203, 10);                   
//
	double tm = omp_get_wtime();                                  //time
//population creating in pop_vect
	vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, stream, 
					  DIM*POP_SIZE, pop_vect, -M_PI, M_PI);
	
	
						 
//starting population better solution
							       		    //CHANGE IF FUNC CHANGED
minimum = sin(pop_vect[0])*sin(pop_vect[POP_SIZE])*sin(pop_vect[2*POP_SIZE]);
    #pragma omp parallel for reduction(min: minimum) schedule(static)
	for (i = 1; i < POP_SIZE; i++)
	{
			                                                    //CHANGE IF FUNC CHANGED
		if ((y = sin(pop_vect[i]) * sin(pop_vect[i + POP_SIZE]) *
			sin(pop_vect[i + (2 * POP_SIZE)])) < minimum)
		{
			minimum = y;
		}
	}
	
	
//main cycle 
	for (step = 0; step < step_count; step++)
	{
//cycle for changing pop_vect
		for (current_iter = 0; current_iter < ITER_COUNT; current_iter++)
		{
			vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, stream, 
								DIM * POP_SIZE * 2, rand_vect, -7, 7);
//genetic algorithm
			for (i = 0; i < DIM; i++)
		    #pragma omp parallel for 
				for (j = 0; j < POP_SIZE - 1; j += 2)
				{
//crossing
				pop_vect[i*DIM + j] = floor(pop_vect[i*DIM + j]) + 
				    (pop_vect[i*DIM +j+1] -floor(pop_vect[i*DIM +j+1]));
				pop_vect[i*DIM +j+1] = floor(pop_vect[i*DIM +j+1]) + 
				    (pop_vect[i*DIM + j] -floor(pop_vect[i*DIM + j]));
//mutation (mutation is absent if rand_vect[pj_xi] <= 0)
				if (rand_vect[i*DIM + j] > 0)
				{
					double chck = pop_vect[i*DIM + j] / 10;
					int k, rnd = 1, count = 0;
					while (fabs(chck) > 1 )
					{
						count++;
						chck /= 10;
					}
					if (count >=  rand_vect[i*DIM + j])
						count = floor(rand_vect[i*DIM + j]);
					else count = floor(rand_vect[i*DIM + j]) - count;
					for (k = 0; k < count; k++)
					{
						rnd *= 10;
					}
					pop_vect[i*DIM + j] = floor(pop_vect[i*DIM + j] *
					    (rnd - 1)) / (rnd - 1) + (pop_vect[i*DIM + j] - 
					    (floor(pop_vect[i*DIM + j] * (rnd + 1)) / 
					    (rnd + 1))) + fabs(rand_vect[i*DIM + j+1]) /rnd;
				}
				if (rand_vect[i*DIM + j+2] > 0)
				{
					double chck = pop_vect[i*DIM + j+1] / 10;
					int k, rnd = 1, count = 0;
					while (fabs(chck) > 1 )
					{
						count++;
						chck /= 10;
					}
					if (count >=  rand_vect[i*DIM + j+2])
						count = floor(rand_vect[i*DIM + j+2]);
					else count = floor(rand_vect[i*DIM + j+2]) - count;
					for (k = 0; k < count; k++)
					{
						rnd *= 10;
					}
					pop_vect[i*DIM + j+1] = floor(pop_vect[i*DIM +j+1] *
					    (rnd - 1)) / (rnd - 1) + (pop_vect[i*DIM +j+1] - 
					    (floor(pop_vect[i*DIM + j+1] * (rnd + 1)) / 
					    (rnd + 1))) + fabs(rand_vect[i*DIM + j+3]) /rnd;
				}
			}
//better solution checking after iteration
			#pragma omp parallel for reduction(min : minimum) schedule(static)
				for (i = 0; i < POP_SIZE; i++)
				{
				if ((y = sin(pop_vect[i]) * sin(pop_vect[i + POP_SIZE]) *
				sin(pop_vect[i + (2 * POP_SIZE)])) < minimum)
				{
					minimum = y;
				}
			}
		}
//changing
		int dest, source;
		dest = (rank == N_MAX - 1)? 0 : (rank + 1);
		source = (rank == 0)? (N_MAX - 1) : (rank - 1);
		MPI_Sendrecv(pop_vect, DIM*POP_SIZE, MPI_DOUBLE, dest, 0, new_pop_vect, 
		DIM*POP_SIZE, MPI_DOUBLE, source, 0, MPI_COMM_WORLD, status);
		tmp_buf = pop_vect;
		pop_vect = new_pop_vect;
		new_pop_vect = tmp_buf;
	}
	MPI_Reduce(&minimum, &result, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
        vslDeleteStream(&stream);
	free(pop_vect);
	free(new_pop_vect);
	free(rand_vect);
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0)
        {
            printf("Local minimum: %lf \n", result);
            printf("Time: %lf \n", omp_get_wtime() - tm);
            printf("Iteration count: %d \n", ITER_COUNT*STEP_COUNT);
        }
	MPI_Finalize();
	return 0;
}
