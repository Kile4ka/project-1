      !{subroutine for generating new random for each time}
      subroutine init_random_seed()
          integer :: i, n, clock
          integer, dimension(:), allocatable :: seed
          call random_seed(size = n)
          allocate(seed(n))
          call system_clock(count = clock)
          seed = clock + 37 * (/ (i - 1, i = 1, n) /)
          call random_seed(put = seed)
          deallocate(seed)
      end subroutine init_random_seed
      
      !{Main program}
      program Trigiogonal_algorithm
      implicit none
      real(8), allocatable::DL(:), DU(:),D(:), B(:,:), X(:,:), HLPR(:,:)  
      real(8), allocatable::DLA(:), DUA(:),DA(:)
      integer, parameter::  NRHS = 3, N1 = 16777216, N2 = 33554432 !2^24, 2^25 
      integer INFO, curr_string, curr_clmn
      real(8) result_b, result_sum, delta
      real t1, t2, stop_, start_
      
      allocate(D(N1), DU(N1-1), DL(N1-1), B(N1, NRHS), X(N1, NRHS))
      
      !generating matrix
      call init_random_seed()
      call RANDOM_NUMBER(DL(:))
      call RANDOM_NUMBER(DU(:))
      call RANDOM_NUMBER(B(:,:))
      B = 100*B
      DU = 1000*DU
      DL = 1000*DL
      
      !diagonal domination
      do curr_string = 2, N1 - 1, 1  
          D(curr_string) = (3*DL(curr_string - 1)) + (3*DU(curr_string))
      end do
      D(1) = 3*DU(1)
      D(N1) = 3*DL(N1-1)
      
      call CPU_TIME(start_)
      call DGTSV(N1, NRHS, DL, D, DU, X, N1, INFO)
      call CPU_TIME(stop_)
      t1 = stop_ - start_
      print *, "Time of computing matrix 2^24 x 2^24: ", t1
      deallocate(DL, DU, D, X, B)
      
      !{Other matrix}
      allocate(D(N2), HLPR(N2,NRHS), DA(N2), B(N2, NRHS), X(N2, NRHS))
      allocate(DU(N2-1), DUA(N2-1), DL(N2-1), DLA(N2-1))
      
      !generating matrix
      call init_random_seed()
      call RANDOM_NUMBER(DL(:))
      call RANDOM_NUMBER(DU(:))
      call RANDOM_NUMBER(B(:,:))
      B = 100*B
      DU = 1000*DU
      DL = 1000*DL
      
      !diagonal domination
      do curr_string = 2, N2 - 1, 1  
          D(curr_string) = (3*DL(curr_string - 1)) + (3*DU(curr_string))
      end do
      D(1) = 3*DU(1)
      D(N2) = 3*DL(N2-1)
      
      X = B
      DLA = DL
      DUA = DU
      DA = D
      call CPU_TIME(start_)
      call DGTSV(N2, NRHS, DL, D, DU, X, N2, INFO)
      call CPU_TIME(stop_)
      t2 = stop_ - start_ 
      print *, "Time of computing matrix 2^25 x 2^25: ", t2
      delta = N2 / N1
      print *, "Practical complexity is n^", log(t2/t1) / log (delta)
      
      !{Asymptotic stability analysis}
      HLPR(1,:) = DA(1)*X(1,:) + DUA(1)*X(2,:) - B(1,:)
      do curr_string = 2, N2 - 1, 1
          HLPR(curr_string, :) = DA(curr_string)*X(curr_string,:) +    &
          DLA(curr_string-1)*X(curr_string-1,:) + DUA(curr_string) *   &
          X(curr_string+1,:) - B(curr_string,:)
      end do
      HLPR(N2,:) = (DA(N2)*X(N2,:)) + (DLA(N2-1)*X(N2-1,:)) - B(N2,:)
      
      result_b = 0
      result_sum = 0
      do curr_string = 1, N2, 1
      do curr_clmn = 1, NRHS, 1
          result_sum = result_sum + (HLPR(curr_string, curr_clmn) *    &
          HLPR(curr_string, curr_clmn))
          result_b = result_b + (B(curr_string, curr_clmn) *           &
          B(curr_string, curr_clmn))
      end do
      end do
      print *, "Relative error is ", (result_sum / result_b)
      
      deallocate(DL, DU, D, X, B, DLA, DUA, DA, HLPR)
      
      end program Trigiogonal_algorithm
