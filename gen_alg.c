#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <mkl.h>

/*This algorithm works with constant function 
 * if you want to change function you have to
 * change some parts of the programm 
 * Current function is << y(x) = sin(x)*sin(y)*sin(z) >>  //CHANGE IF FUNC CHANGED!!!
 * This programm founds << minimum >> of the function
 * pop_vect is p1_x1, p2_x1, ... , p10_x1, p1_x2, ... , p10_xn*/
 
enum 
{
	DIM = 3,                                        //function dimention
	POP_SIZE = 10000,                                     //population size
	ITER_COUNT = 2000,                                  //break criterion
};


int main()
{
	double *pop_vect = (double*)malloc(sizeof(double)*(DIM*POP_SIZE)), *rand_vect = (double*)malloc(sizeof(double)*(DIM*POP_SIZE*2));
	int i, j, current_iter = 0;
	double minimum, y; 
//random
	VSLStreamStatePtr stream;
	vslNewStream(&stream, VSL_BRNG_MT2203, 10);                   
//
	double tm = omp_get_wtime();                                  //time
//population creating in pop_vect
	  vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, stream, 
						  DIM*POP_SIZE, pop_vect, -M_PI, M_PI);
						 
//starting populaton better solution
							       		    //CHANGE IF FUNC CHANGED
minimum = sin(pop_vect[0])*sin(pop_vect[POP_SIZE])*sin(pop_vect[2*POP_SIZE]);
        #pragma omp parallel for reduction(min: minimum) schedule(static) //<-------------------------
	for (i = 1; i < POP_SIZE; i++)
	{
			                                                    //CHANGE IF FUNC CHANGED
		if ((y = sin(pop_vect[i]) * sin(pop_vect[i + POP_SIZE]) *
			sin(pop_vect[i + (2 * POP_SIZE)])) < minimum)
		{
			minimum = y;
		}
	}
//main cycle
	for (current_iter = 0; current_iter < ITER_COUNT; current_iter++)
	{
		vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, stream, 
								DIM * POP_SIZE * 2, rand_vect, -7, 7);
//genetic algorithm
		
		for (i = 0; i < DIM; i++)
		  #pragma omp parallel for  //<--------------------------------------------
			for (j = 0; j < POP_SIZE - 1; j += 2)
			{
//crossing
				pop_vect[i*DIM + j] = floor(pop_vect[i*DIM + j]) + 
				    (pop_vect[i*DIM +j+1] -floor(pop_vect[i*DIM +j+1]));
				pop_vect[i*DIM +j+1] = floor(pop_vect[i*DIM +j+1]) + 
				    (pop_vect[i*DIM + j] -floor(pop_vect[i*DIM + j]));
//mutation (mutation is absent if rand_vect[pj_xi] <= 0)
				if (rand_vect[i*DIM + j] > 0)
				{
					double chck = pop_vect[i*DIM + j] / 10;
					int k, rnd = 1, count = 0;
					while (fabs(chck) > 1 )
					{
						count++;
						chck /= 10;
					}
					if (count >=  rand_vect[i*DIM + j])
						count = floor(rand_vect[i*DIM + j]);
					else count = floor(rand_vect[i*DIM + j]) - count;
					for (k = 0; k < count; k++)
					{
						rnd *= 10;
					}
					pop_vect[i*DIM + j] = floor(pop_vect[i*DIM + j] *
					    (rnd - 1)) / (rnd - 1) + (pop_vect[i*DIM + j] - 
					    (floor(pop_vect[i*DIM + j] * (rnd + 1)) / 
					    (rnd + 1))) + fabs(rand_vect[i*DIM + j+1]) /rnd;
				}
				if (rand_vect[i*DIM + j+2] > 0)
				{
					double chck = pop_vect[i*DIM + j+1] / 10;
					int k, rnd = 1, count = 0;
					while (fabs(chck) > 1 )
					{
						count++;
						chck /= 10;
					}
					if (count >=  rand_vect[i*DIM + j+2])
						count = floor(rand_vect[i*DIM + j+2]);
					else count = floor(rand_vect[i*DIM + j+2]) - count;
					for (k = 0; k < count; k++)
					{
						rnd *= 10;
					}
					pop_vect[i*DIM + j+1] = floor(pop_vect[i*DIM +j+1] *
					    (rnd - 1)) / (rnd - 1) + (pop_vect[i*DIM +j+1] - 
					    (floor(pop_vect[i*DIM + j+1] * (rnd + 1)) / 
					    (rnd + 1))) + fabs(rand_vect[i*DIM + j+3]) /rnd;
				}
			}
//better solution checking after iteration
                #pragma omp parallel for reduction(min : minimum) schedule(static) //<-------------
		for (i = 0; i < POP_SIZE; i++)
		{
			if ((y = sin(pop_vect[i]) * sin(pop_vect[i + POP_SIZE]) *
			sin(pop_vect[i + (2 * POP_SIZE)])) < minimum)
			{
				minimum = y;
			}
		}
	}
	printf("Local minimum: %lf \n", minimum);
	printf("Iteration count: %d \n", current_iter);
	printf("Time: %lf \n", omp_get_wtime() - tm);
        vslDeleteStream(&stream);
	free(pop_vect);
	free(rand_vect);
	return 0;
}
