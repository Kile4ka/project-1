      program Trigiogonal_algorithm
      implicit none
      real, allocatable:: gamma(:), alpha(:), beta(:,:), right(:),x(:,:)
      integer n_matrix, n_x, current_index, current_string
      real c1, c2, c3, nulls
      
      print *,"Put the size of your system"
      read *,n_matrix
      print *,"Put the size of x"
      read *,n_x
      
      allocate(gamma(n_matrix))
      allocate(alpha(n_matrix))
      allocate(beta(n_matrix, n_x))
      allocate(right(n_x))
      allocate(x(n_matrix,n_x))
      
      print *,"Put extended tridiagonal matrix of your system"
      
      !{Forward}
      
      !read 1st string
      read (*, *) gamma(1), c3
      do current_index = 3, n_matrix, 1 !nulls
        read *, nulls
      end do
      read *, right
      !computing auxiliary variables for 1 string
      alpha(1) = -(c3 / gamma(1));
      beta(1, :) = right / gamma(1)
      
      !other strings
      do current_string = 2, n_matrix, 1
        do current_index = 1, current_string - 1, 1 !nulls then c1
          read *,c1
        end do
        read *, c2
        read *, c3
        do current_index = current_string + 2, n_matrix, 1 !nulls
          read *,nulls
        end do
        if (current_string == n_matrix) then
            right(1) = c3;
            do current_index = 2, n_x, 1
              read *, right(current_index)
            end do
        else
            read *, right
        end if
        !computing auxiliary variables
        gamma(current_string) = c2 + (c1 * alpha(current_string - 1))
        alpha(current_string) = -(c3 / gamma(current_string))
        beta(current_string, :) = (right - (c1 *                       &
        beta(current_string - 1, :))) / gamma(current_string)         
      end do
      
      !{Revers}
      
      x(n_matrix, :) = beta(n_matrix, :)
      do current_string = n_matrix - 1, 1, -1
        x(current_string, :) = alpha(current_string) *                 &
        x(current_string + 1, :) + beta(current_string, :)
      end do 
      
      !print desision
      print *, "Desision of your system:"
      do current_string = 1, n_matrix, 1
        print *, x(current_string, :)
      end do

      deallocate(alpha)
      deallocate(x)
      deallocate(beta)
      deallocate(gamma)
      deallocate(right)
      
      end program Trigiogonal_algorithm
