
//-----------{Tridiagonal matrix algorithm}-----
//---------------{version without tests}--------------
//------(work with real type of coefficients)------
//-----------(and with big right part)---------

#include <stdio.h>
#include <stdlib.h>

#define my_type double


int main()
{
    int n_matrix, n_x, current_index;
    printf("Put the size of your system\n");
    scanf("%i", &n_matrix);
    printf("Put the size of x\n");
    scanf("%i", &n_x);
    
    //select memory for x and auxiliary variables
    double *_gamma = (my_type*)malloc(sizeof(double)*n_matrix);
    double *_alpha = (my_type*)malloc(sizeof(double)*n_matrix);
    double *_beta = (my_type*)malloc(sizeof(double)*n_matrix*n_x);
    double *_x = (my_type*)malloc(sizeof(double)*n_matrix*n_x);
    my_type *_right = (my_type*)malloc(sizeof(my_type)*n_x); //for big right part
    
    //check if we got enough memory
    if ((_gamma == NULL) || (_alpha == NULL) || (_beta == NULL) || (_x == NULL) || (_right == NULL))
    {
        printf("Not enough memory. Try to use more powerful machine.\n");
        return -1;
    }
    
    //-------------{Forward}-----------------
    
    my_type _c1, _c2, _c3, _nulls;
    printf("Put extended tridiagonal matrix of coefficients of your system\n");
    //read first string
    scanf("%lf %lf", &_gamma[0], &_c3); //first gamma
    for (int i = 2; i < n_matrix; i++) //nulls
    {
        scanf("%lf", &_nulls);
    }
    for (int i = 0; i < n_x; i++)
    {
        scanf("%lf", &_right[i]);
    }
    
    //computing auxiliary variables for 1 string
    _alpha[0] = -(_c3/_gamma[0]);
    for (current_index = 0; current_index < n_x; current_index++)
    {
        _beta[current_index] = _right[current_index] / _gamma[0];
    }
    //read other strings
    for (int i = 1; i < n_matrix; i++)
    {
        for (int j = 0; j < i; j++)
        {
            scanf("%lf", &_c1);
        }
        scanf("%lf", &_c2);
        scanf("%lf", &_c3);
        for (int j = i + 2; j < n_matrix; j++) //nulls
        {
            scanf("%lf", &_nulls);
        }
        if (i + 2 > n_matrix)
        {
            _right[0] = _c3;
            for (int k = 1; k < n_x; k++)
            {
                scanf("%lf", &_right[k]);
            }
        }
        else for (int k = 0; k < n_x; k++)
        {
            scanf("%lf", &_right[k]);
        }
    
        //computing auxiliary variables
        _gamma[i] = _c2 + (_c1*_alpha[i-1]);
        _alpha[i] = -(_c3/_gamma[i]);
        for (int k = 0; k < n_x; k++)
        {
            _beta[current_index] = ((_right[k] - (_c1*_beta[current_index - n_x]))/_gamma[i]);
            current_index++;
        }
    }
    current_index--; 
    
    //---------{Revers}---------
    
    for (int i = 0; i < n_x; i++) //x_n = ...
    {
        _x[current_index] = _beta[current_index];
        current_index--;
    }
     //computing x_i
    for (int i = (n_matrix - 2); i >= 0; i--)
    {
        for (int k = 0; k < n_x; k++)
        {
            _x[current_index] = _alpha[i]*_x[current_index + n_x] + _beta[current_index]; 
            current_index--;
        }
    }
    
    //write desision
    printf("Desision of your system:\n");
    for (int i = 0; i < n_matrix; i++)
    {
        printf("x%i = ", i+1);
        for (int j = 0; j < n_x; j++)
        {
            printf("%lf ", _x[i*n_x + j]);
        }
        printf(";\n");
    }

    //memory free
    free(_alpha);
    free(_beta);
    free(_gamma);
    free(_x);
    free(_right);
    _alpha = _beta = _gamma = _x = _right = NULL;
    
    return 0;
}
