      !{subroutine for generating new random for each time}
      subroutine init_random_seed()
          integer :: i, n, clock
          integer, dimension(:), allocatable :: seed
          call random_seed(size = n)
          allocate(seed(n))
          call system_clock(count = clock)
          seed = clock + 37 * (/ (i - 1, i = 1, n) /)
          call random_seed(put = seed)
          deallocate(seed)
      end subroutine init_random_seed
      
      !{Main program}
      program Tridiogonal_algorithm
      implicit none
      real(8), allocatable:: gamma(:), alpha(:), beta(:,:), X(:,:)
      real(8), allocatable:: DL(:), DU(:), D(:), B(:,:), HLPR(:,:) 
      integer, parameter:: NRHS = 3, N1 = 16777216, N2 = 33554432  !2^24, 2^25
      integer curr_string, curr_clmn
      real(8)  result_sum, result_b, delta
      real t1, t2, start_, stop_
      
      allocate(gamma(N1), alpha(N1-1), beta(N1,NRHS), X(N1,NRHS), D(N1))
      allocate(DU(N1-1), DL(N1-1), B(N1,NRHS))
      
      !generating matrix
      call init_random_seed()
      call RANDOM_NUMBER(DL(:))
      call RANDOM_NUMBER(DU(:))
      call RANDOM_NUMBER(B(:,:))
      B = 1000*B
      DU = 1000*DU
      DL = 1000*DL
      
      !diagonal dominance
      do curr_string = 2, N1 - 1, 1  
          D(curr_string) = (3*DL(curr_string - 1)) + (3*DU(curr_string))
      end do
      D(1) = 3*DU(1)
      D(N1) = 3*DL(N1-1)
      
      !{Forward}
      
      call CPU_TIME(start_)
      ! 1st string
      gamma(1) = D(1)
      alpha(1) = -(DU(1) / gamma(1))
      beta(1,:) = B(1,:) / gamma(1)
      
      !other strings
      do curr_string = 2, N1, 1
          gamma(curr_string) = D(curr_string) + (DL(curr_string - 1) * &
          alpha(curr_string - 1))
          if (curr_string < N1) then
          alpha(curr_string) = -(DU(curr_string) / gamma(curr_string))
          endif
          beta(curr_string,:) = (B(curr_string,:) - (DL(curr_string -  &
          1) * beta(curr_string - 1,:))) / gamma(curr_string)         
      end do
      
      !{Revers}
      x(N1,:) = beta(N1,:)
      do curr_string = N1 - 1, 1, -1
        x(curr_string,:) = alpha(curr_string) * x(curr_string + 1,:) + &
        beta(curr_string,:)
      end do 
      call CPU_TIME(stop_)
      
      t1 = stop_ - start_
      print *, "Time for n = 2^24 is ", t1
      deallocate(alpha, x, beta, gamma, DU, DL, D, B)
      
      !{Other matrix}
      
      allocate(gamma(N2), alpha(N2-1), beta(N2,NRHS), X(N2,NRHS), D(N2))
      allocate(DU(N2-1), DL(N2-1), B(N2, NRHS), HLPR(N2, NRHS))
      
      !generating matrix
      call init_random_seed()
      call RANDOM_NUMBER(DL(:))
      call RANDOM_NUMBER(DU(:))
      call RANDOM_NUMBER(B(:,:))
      B = 1000*B
      DU = 1000*DU
      DL = 1000*DL
      
      do curr_string = 2, N2 - 1, 1  
          D(curr_string) = (3*DL(curr_string - 1)) + (3*DU(curr_string))
      end do
      D(1) = 3*DU(1)
      D(N2) = 3*DL(N2-1)
      
      !{Forward}
      call CPU_TIME(start_)
      
      ! 1st string
      gamma(1) = D(1)
      alpha(1) = -(DU(1) / gamma(1))
      beta(1,:) = B(1,:) / gamma(1)
      
      !other strings
      do curr_string = 2, N2, 1
          gamma(curr_string) = D(curr_string) + (DL(curr_string - 1) * &
          alpha(curr_string - 1))
          if (curr_string < N2) then
          alpha(curr_string) = -(DU(curr_string) / gamma(curr_string))
          endif
          beta(curr_string,:) = (B(curr_string,:) - (DL(curr_string -  &
          1) * beta(curr_string - 1,:))) / gamma(curr_string)         
      end do
      
      !{Revers}
      x(N2,:) = beta(N2,:)
      do curr_string = N2 - 1, 1, -1
        x(curr_string,:) = alpha(curr_string) * x(curr_string + 1,:) + &
        beta(curr_string,:)
      end do 
      call CPU_TIME(stop_)
      t2 = stop_ - start_
      print *, "Time for n = 2^25 is ", t2
      delta = N2 / N1
      print *, "Practical complexity is n^", log(t2/t1) / log (delta)
      
      !{Asymptotic stability analysis}
      HLPR(1,:) = D(1)*X(1,:) + DU(1)*X(2,:) - B(1,:)
      do curr_string = 2, N2 - 1, 1
          HLPR(curr_string, :) = D(curr_string)*X(curr_string,:) +    &
          DL(curr_string-1)*X(curr_string-1,:) + DU(curr_string) *   &
          X(curr_string+1,:) - B(curr_string,:)
      end do
      HLPR(N2,:) = (D(N2)*X(N2,:)) + (DL(N2-1)*X(N2-1,:)) - B(N2,:)
      
      result_b = 0
      result_sum = 0
      do curr_string = 1, N2, 1
      do curr_clmn = 1, NRHS, 1
          result_sum = result_sum + (HLPR(curr_string, curr_clmn) *    &
          HLPR(curr_string, curr_clmn))
          result_b = result_b + (B(curr_string, curr_clmn) *           &
          B(curr_string, curr_clmn))
      end do
      end do
      print *, "Relative error is:"
      write (*, '(F100.70)')dsqrt(result_sum / result_b)
      
      deallocate(alpha, x, beta, gamma, DU, DL, D, B)

      end program Tridiogonal_algorithm
