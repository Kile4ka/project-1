
//---------{Tridiagonal matrix algorithm}------------
//---------{version with automatic tests}------------

//----integer variables are used for the testing-----
//----just becouse its easier to work with "rand()"--

//----testing is for the matrixes with "small" right side

#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#include <math.h> 

#define my_type int

enum {N1 = 16777216, N2 = 67108864, N3 = 33554432, MAX_VALUE = 10000};
// N1 = 2^24, N2 = 2^26, N3 = 2^25 33554432

int main()
{
    
    my_type _c1, _c2, _c3, _d;
    int _sign; //for sign
    clock_t start, stop, t1, t2;
    srand(time(NULL)); //generating different value in different runs
    
    
    //--------{Practical complexity of the method analisys}
    //------------------{First matrix (2^24)}--------------
    
    //select memory for x and auxiliary variables
    double *_gamma = malloc(sizeof(double) * N1);
    double *_alpha = malloc(sizeof(double) * N1);
    double *_beta = malloc(sizeof(double) * N1);
    double *_x = malloc(sizeof(double) * N1);
    
    //check if we got enough memory
    if ((_gamma == NULL) || (_alpha == NULL) || (_beta == NULL) || (_x == NULL))
    {
        printf("Not enough memory. Try to use more powerful machine.\n");
        return -1;
    }
    
    //-------------{Forward}-----------------
    
    start = clock(); //starting the computation
    //generating random 
    
    _sign = 1 + rand() % 2;
    _c3 = 1 + rand() % (MAX_VALUE / 4);          // generating diogonal dominance
    if (_sign % 2 != 0) _c3 = -_c3;             // generating random sign
    _c2 = _gamma[0] = abs(_c3) * 4 + rand() % MAX_VALUE;
    _sign = 1 + rand() % 2;
    _d = rand() % MAX_VALUE;
    if (_sign % 2 != 0) _d = -_d; 
    
    //select memory for x and auxiliary variables for 1 string
    _alpha[0] = -(_c3/_gamma[0]);
    _beta[0] = (_d/_gamma[0]);
    
    //other matrix strings
    for (int i = 1; i < N1; i++)
    {
        //generating coefficients != 0
        _sign = 1 + rand() % 2;
        _c1 = rand() % (RAND_MAX / 8); 
        if (_sign % 2 != 0) _c1 = -_c1; 
        _sign = 1 + rand() % 2;
        _c3 = 1 + rand() % (RAND_MAX / 8);
        if (_sign % 2 != 0) _c3 = -_c3; 
        _sign = 1 + rand() % 2;
        _c2 = (abs(_c3) + abs(_c1)) * 2 + rand() % MAX_VALUE ;
        if (_sign % 2 != 0) _c2 = -_c2;  
        _sign = 1 + rand() % 2;
        _d = 1 + rand() % MAX_VALUE;
        if (_sign % 2 != 0) _d = -_d;
        
        //computing auxiliary variables
        _gamma[i] = _c2 + (_c1*_alpha[i-1]);
        _alpha[i] = -(_c3/_gamma[i]);
        _beta[i] = ((_d - (_c1*_beta[i-1]))/_gamma[i]);
    }
    
    //---------{Revers}---------
    
    _x[N1 - 1] = _beta[N1 - 1];
    for (int i = N1 -2; i >= 0; i--)
    {
        _x[i] = _alpha[i]*_x[i+1] + _beta[i]; //computing x_i
    }

    stop = clock(); //the end of computing
    printf("Time for matrix 2^24: t1 = %ld\n", t1 = (stop - start) / CLOCKS_PER_SEC);
    free(_alpha);
    free(_beta);
    free(_gamma);
    free(_x);
    _alpha = _beta = _gamma = _x = NULL;
    
    //---------------{Second matrix (2^26)}--------------
    //------------(code is the same as higher)-----------
    
    //select memory for x and auxiliary variables
    _gamma = malloc(sizeof(double) * N2);
    _alpha = malloc(sizeof(double) * N2);
    _beta = malloc(sizeof(double) * N2);
    _x = malloc(sizeof(double) * N2);
    
    //check if we got enough memory
    if ((_gamma == NULL) || (_alpha == NULL) || (_beta == NULL) || (_x == NULL))
    {
        printf("Not enough memory. Try to use more powerful machine.\n");
        return -1;
    }
    
    //-------------{Forward}-----------------
    
    start = clock(); //start of computing
    
    //generating coefficientts != 0 for 1st string
    _sign = 1 + rand() % 2;
    _c3 = 1 + rand() % (RAND_MAX / 4); 
    if (_sign % 2 != 0) _c3 = -_c3;        
    _sign = 1 + rand() % 2;
    _c2 = _gamma[0] = (abs(_c3) * 2) + rand() % MAX_VALUE;
    if (_sign % 2 != 0) _c2 = -_c2;  
    _sign = 1 + rand() % 2;
    _d = 1 + rand() % MAX_VALUE;
    if (_sign % 2 != 0) _d = -_d; 
    
    //select memory for x and auxiliary variables for 1 strring
    _alpha[0] = -(_c3/_gamma[0]);
    _beta[0] = (_d/_gamma[0]);
    
    //other matrix strings
    for (int i = 1; i < N2; i++)
    {
        //generating coefficientts != 0 for other strings
        _sign = 1 + rand() % 2;
        _c1 = 1 + rand() % (RAND_MAX / 8); 
        if (_sign % 2 != 0) _c1 = -_c1; 
        _sign = 1 + rand() % 2;
        _c3 = 1 + rand() % (RAND_MAX / 8);
        if (_sign % 2 != 0) _c3 = -_c3; 
        _sign = 1 + rand() % 2;
        _c2 = (abs(_c3) + abs(_c1)) * 2 + rand() % MAX_VALUE;
        if (_sign % 2 != 0) _c2 = -_c2;
        _sign = 1 + rand() % 2;
        _d = 1 + rand() % MAX_VALUE;
        if (_sign % 2 != 0) _d = -_d;
        
        //computing auxiliary variables
        _gamma[i] = _c2 + (_c1*_alpha[i-1]);
        _alpha[i] = -(_c3/_gamma[i]);
        _beta[i] = ((_d - (_c1*_beta[i-1]))/_gamma[i]);
    }
    
    
    //---------{Revers}---------
    
    _x[N2 - 1] = _beta[N2 - 1]; // x_n = ...
    for (int i = N2 -2; i >= 0; i--)
    {
        _x[i] = _alpha[i]*_x[i+1] + _beta[i]; //computing x_i
    }
    stop = clock(); //the end of computing
    printf("Time for matrix 2^26: t2 = %ld\n", t2 = (stop - start) / CLOCKS_PER_SEC);
    free(_alpha);
    free(_beta);
    free(_gamma);
    free(_x);
    _alpha = _beta = _gamma = _x = NULL;
    double T = t2/t1;
    //for correct work foloowing string use '-lm' with other build commands
    printf("Practical complexity of the method: n^%lf\n", (log(T))/(log(N2/N1)));
    
    
    
    //-----------{Asymptotic stability analysis}-----
    //----------------{use a matrix N3 = 2^25}-------
    
    
    //select memory for x and auxiliary variables    
    my_type *_matrix = malloc((3*N3 - 2) * sizeof(double));
    my_type *_right = malloc(N3 * sizeof(my_type));
    _gamma = malloc(sizeof(double) * N3);
    _alpha = malloc(sizeof(double) * N3);
    _beta = malloc(sizeof(double) * N3);
    _x = malloc(sizeof(double) * N3);
    
    //check if we got enough memory
    if ((_gamma == NULL) || (_alpha == NULL) || 
        (_beta == NULL) || (_x == NULL) || (_matrix == NULL) || (_right == NULL))
    {
        printf("Not enough memory. Try to use more powerful machine.\n");
        return -1;
    }
    
    //generating random coefficients for 1 string
    _sign = 1 + rand() % 2;
    _c3 = (1 + rand() % ((MAX_VALUE/4) - 1));       // genereting diogonal dominance
    if (_sign % 2 != 0) _c3 = -_c3;                 // generating random sign
    _c2 = _gamma[0] = (abs(_c3) * 4 + rand() % MAX_VALUE);
    _sign = 1 + rand() % 2;
    _d = 1 + rand() % MAX_VALUE;
    if (_sign % 2 != 0) _d = -_d; 
    _matrix[0] = _c2;
    _matrix[1] = _c3;
    _right[0] = _d;
    
    //computing values for auxiliary variables
    _alpha[0] = -(_c3/_gamma[0]);
    _beta[0] = (_d/_gamma[0]);
    
    int matrix_index = 3;
    //other strings
    for (int i = 1; i < N3; i++)
    {
        //geterating coefficients != 0
        _sign = 1 + rand() % 2;
        _c1 = 1 + rand() % (MAX_VALUE / 8); 
        if (_sign % 2 != 0) _c1 = -_c1; 
        _sign = 1 + rand() % 2;
        _c3 = 1 + rand() % (MAX_VALUE / 8);
        if (_sign % 2 != 0) _c3 = -_c3; 
        _sign = 1 + rand() % 2;
        _c2 = ((abs(_c3) + abs(_c1))*4 + rand() % MAX_VALUE);
        if (_sign % 2 != 0) _c2 = -_c2;  
        _sign = 1 + rand() % 2;
        _d = 1 + rand() % MAX_VALUE;
        if (_sign % 2 != 0) _d = -_d;

        _right[i] = _d;
        _matrix[matrix_index - 1] = _c1;
        _matrix[matrix_index] = _c2;
        // !!!
        if (matrix_index < (3*N3 - 2))
            _matrix[matrix_index + 1] = _c3;
        matrix_index += 3;
        
        //computing values for auxiliary variables
        _gamma[i] = _c2 + (_c1*_alpha[i-1]);
        _alpha[i] = -(_c3/_gamma[i]);
        _beta[i] = ((_d - (_c1*_beta[i-1]))/_gamma[i]);
    }
    
    _x[N3 - 1] = _beta[N3 - 1];
    for (int i = N3 - 2; i >= 0; i--)
    {
        _x[i] = _alpha[i]*_x[i+1] + _beta[i];
    }
    
    
    //checking wigh second normas
    matrix_index = 0;
    int i;
    char flag_first_str = 1;
    long double _result11 = 0, _result12 = 0, _result2 = 0, _result = 0;
    for (i = 0; i < N3 - 1; i++)
    {
        if (!flag_first_str)
        {
            _result11 = _result11 + (_matrix[matrix_index]*_x[i - 1]);
            matrix_index++;
        }
        _result11 = _result11 + (_matrix[matrix_index]*_x[i]);
        matrix_index++;
        _result11 = _result11 + (_matrix[matrix_index]*_x[i + 1]);
        matrix_index++; 
        flag_first_str = 0;
        _result11 = _result11 - _right[i];
        _result12 = _result12 + (_result11*_result11);
        _result11 = 0;
    }
    
    //last string
    _result11 = _result11 + (_matrix[matrix_index]*_x[i - 1]);
    matrix_index++;
    _result11 = _result11 + (_matrix[matrix_index]*_x[i]);
    _result11 = _result11 - _right[i];
    
    _result12 = _result12 + (_result11*_result11);
    
    
    
    for (i = 0; i < N3; i++)
    {
        
        _result2 = _result2 + (_right[i]*_right[i]);
    }
    _result = sqrt(_result12) / sqrt(_result2);
    printf("Relative error: delta = %-50.20llf", _result);
    
    
    free(_matrix);
    free(_right);
    free(_x);
    free(_gamma);
    free(_alpha);
    free(_beta);
    return 0;
}

