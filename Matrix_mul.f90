        program Mat_Mul
        implicit none
        integer, parameter:: n = 2000, blocks = 40, block_size = 50
        real matrix_1(n,n), current_1(block_size, block_size) 
        real current_2(block_size, block_size), matrix_2(n,n), res(n,n)
        real curr_res(block_size, block_size)
        integer i, j, k, helper, curr_string, curr_column
        real start_time, stop_time
        
        ! initialise
        do i = 1, N, 1
            do j = 1, N, 1
                call RANDOM_NUMBER(matrix_1(i,j))
            end do
        end do
        do i = 1, N, 1
            do j = 1, N, 1
                call RANDOM_NUMBER(matrix_2(i, j))
            end do
        end do
        
        
        call CPU_TIME(start_time)
        res = MATMUL(matrix_1, matrix_2)
        call CPU_TIME(stop_time)
        print *,  "Matmul time: ", stop_time - start_time
        
        
        call CPU_TIME(start_time)
        do curr_string = 1, blocks, 1 
        do curr_column = 1, blocks, 1
        do helper = 1, blocks, 1
            do i = 1, block_size, 1
            do j = 1, block_size, 1
            current_1(i, j) = matrix_1((curr_string-1)*block_size+i,(helper-1)*block_size+j)
            current_2(i, j) = matrix_2((helper-1)*block_size+i,(curr_column-1)*block_size+j)
            curr_res(i,j) = 0;
            end do
            end do
            
            do i = 1, block_size, 1
            do j = 1, block_size, 1
            do k = 1, block_size, 1
            curr_res(i,j) = curr_res(i,j) + current_1(i,k)*current_2(k,j)
            end do
            end do
            end do
        end do    
        do i = 1, block_size, 1
        do j = 1, block_size, 1
        res((curr_string-1)*block_size+i,(curr_column-1)*block_size+j) = curr_res(i,j)
        end do
        end do
        end do
        end do
        call CPU_TIME(stop_time)
        print *, "Blocks time: ", stop_time - start_time
        
        end program Mat_Mul
        
